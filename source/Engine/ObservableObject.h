#pragma once

#include "DrawableGameComponent.h"

using namespace Library;
namespace Library
{
	class ObservableObject : public DrawableGameComponent
	{
		RTTI_DECLARATIONS( ObservableObject, DrawableGameComponent )

	public:
		ObservableObject( Game& game, Camera& camera );
		~ObservableObject();

		virtual void Update( const GameTime& gameTime ) override;

		const XMFLOAT3& Position() const;
		const XMFLOAT3& Velocity() const;

		virtual void SetScaleCoeff( float scale );
		virtual float GetScaleCoeff();

		virtual std::vector<std::string> ObjectDesc() = 0;
		
	protected:
		ObservableObject();
		ObservableObject( const ObservableObject& rhs );
		ObservableObject& operator=(const ObservableObject& rhs);

		XMFLOAT3 mPosition;
		//XMFLOAT3 mPrevPosition;
		XMFLOAT3 mAverVelocity;


		float scaleCoefficient;
		std::vector<std::string> mObjectDescription;
	};
}

