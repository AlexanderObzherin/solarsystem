#include "ObservableObject.h"
#include "ThirdPersonCamera.h"
#include "Game.h"
#include "GameTime.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "VectorHelper.h"



#include "GameException.h"

namespace Library
{
	RTTI_DEFINITIONS( ThirdPersonCamera )

	const float ThirdPersonCamera::DefaultRotationRate = XMConvertToRadians( 1.0f );
	const float ThirdPersonCamera::DefaultMovementRate = 10.0f;
	const float ThirdPersonCamera::DefaultMouseSensitivity = 100.0f;

	const std::string ThirdPersonCamera::DrawCameraViewPosNames[] = { "Bottom", "-45 deg angle", "Side", "45 deg angle", "Top" };
	const float ThirdPersonCamera::mMaxRotationAngle = 0.00312f;

	ThirdPersonCamera::ThirdPersonCamera( Game& game )
		:
		Camera( game ),
		mKeyboard( nullptr ),
		mMouse( nullptr ),

		mObservableObject( nullptr ),
		mObsrevablePoint( &Vector3Helper::Zero ),

		mTargetPoint( Vector3Helper::Zero ),

		mViewVector( Vector3Helper::Zero ),

		mMouseSensitivity( DefaultMouseSensitivity ),
		mRotationRate( DefaultRotationRate ),
		mMovementRate( DefaultMovementRate ),
		mRotationAngle( 0.0f ),
		mRotationAngleVelocityCoeff( 0.0001f/*0.00003f*/ ),
		mCurrentPolarAngle( 0.0f ),
		targetPolarAngle( -XM_PIDIV2 ),

		mCameraDistance( 30.0f ),
		mCameraViewPosition( top )
	{
	}

	ThirdPersonCamera::ThirdPersonCamera( Game& game, float fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance )
		:
		Camera( game, fieldOfView, aspectRatio, nearPlaneDistance, farPlaneDistance ),
		mKeyboard( nullptr ),
		mMouse( nullptr ),

		mObservableObject( nullptr ),
		mObsrevablePoint( nullptr ),

		mTargetPoint( Vector3Helper::Zero ),

		mViewVector( Vector3Helper::Zero ),

		mMouseSensitivity( DefaultMouseSensitivity ),
		mRotationRate( DefaultRotationRate ),
		mMovementRate( DefaultMovementRate ),

		mRotationAngle( 0.0f ),
		mRotationAngleVelocityCoeff( 0.00003f ),
		mCurrentPolarAngle( 0.0f ),
		targetPolarAngle( -XM_PIDIV2 ),

		mCameraDistance( 50.0f ),
		mCameraViewPosition( top )
	{

	}

	ThirdPersonCamera::~ThirdPersonCamera()
	{
		mKeyboard = nullptr;
		mMouse = nullptr;
	}

	const Keyboard& ThirdPersonCamera::GetKeyboard() const
	{
		return *mKeyboard;
	}

	void ThirdPersonCamera::SetKeyboard( Keyboard& keyboard )
	{
		mKeyboard = &keyboard;
	}

	const Mouse& ThirdPersonCamera::GetMouse() const
	{
		return *mMouse;
	}

	void ThirdPersonCamera::SetMouse( Mouse& mouse )
	{
		mMouse = &mouse;
	}

	float& ThirdPersonCamera::MouseSensitivity()
	{
		return mMouseSensitivity;
	}

	float& ThirdPersonCamera::RotationRate()
	{
		return mRotationRate;
	}

	float& ThirdPersonCamera::MovementRate()
	{
		return mMovementRate;
	}

	void ThirdPersonCamera::Initialize()
	{
		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		mMouse = (Mouse*)mGame->Services().GetService( Mouse::TypeIdClass() );

		Camera::Initialize();

		mViewVector = XMFLOAT3( 0.0f, 0.0f, mCameraDistance *(mObservableObject->GetScaleCoeff()) );
	}

	void ThirdPersonCamera::Update( const GameTime& gameTime )
	{
		XMFLOAT2 orbitMovementAmount = Vector2Helper::Zero;

		//if( mKeyboard != nullptr )
		//{
		//	if( mKeyboard->IsKeyDown( DIK_T ) )
		//	{
		//		orbitMovementAmount.x = -1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_G ) )
		//	{
		//		orbitMovementAmount.x = 1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_F ) )
		//	{
		//		orbitMovementAmount.y = -1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_H ) )
		//	{
		//		orbitMovementAmount.y = 1.0f;
		//	}
		//}

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->WasKeyPressedThisFrame( DIK_Q ) )
			{
				if( mCameraViewPosition != top )
				{
					mCameraViewPosition = CameraViewPosition( mCameraViewPosition + 1 );
				}
			}
			if( mKeyboard->WasKeyPressedThisFrame( DIK_A ) )
			{
				if( mCameraViewPosition != bottom )
				{
					mCameraViewPosition = CameraViewPosition( mCameraViewPosition - 1 );
				}
			}
		}

		if( mCameraViewPosition == bottom )
		{
			targetPolarAngle = XM_PIDIV2;
		}
		if( mCameraViewPosition == angleMinus45deg )
		{
			targetPolarAngle = XM_PIDIV4;
		}
		if( mCameraViewPosition == side )
		{
			targetPolarAngle = 0.0f;
		}
		if( mCameraViewPosition == angle45deg )
		{
			targetPolarAngle = -XM_PIDIV4;
		}
		if( mCameraViewPosition == top )
		{
			targetPolarAngle = -XM_PIDIV2;
		}


		if( (int)(targetPolarAngle * 180 / XM_PI) != (int)(mCurrentPolarAngle * 180 / XM_PI) )
		{
			if( (mCurrentPolarAngle) < (targetPolarAngle) )
			{
				orbitMovementAmount.x = 1.0f;
			}
			else
			{
				orbitMovementAmount.x = -1.0f;
			}
		}

		float elapsedTime = (float)gameTime.ElapsedGameTime();
		//float elapsedTime = 1.0f / 60.0f;

		if (mRotationAngle < mMaxRotationAngle)
		{
			mRotationAngle += XM_PI * static_cast<float>(elapsedTime * mRotationAngleVelocityCoeff);
		}
		else
		{
			mRotationAngle = mMaxRotationAngle;
		}
		
		RadialRotation( mRotationAngle * orbitMovementAmount.x, mRotationAngle * orbitMovementAmount.y );
		mCurrentPolarAngle += mRotationAngle * orbitMovementAmount.x;

		XMVECTOR position = XMLoadFloat3( &mPosition );

		XMVECTOR viewVector = XMLoadFloat3( &mViewVector );
		XMVECTOR observablePoint = XMLoadFloat3( &(mObservableObject->Position()) );
		
		viewVector = viewVector * (mObservableObject->GetScaleCoeff() * 0.5f);
		
		XMVECTOR targetPoint = observablePoint + viewVector;
		XMVECTOR previousTargetPoint = targetPoint;
		XMStoreFloat3( &mTargetPoint, targetPoint );
		
		////
		XMVECTOR vDistance = targetPoint - position;
		vDistance = XMVector3Dot( vDistance, vDistance );
		float fDistance = sqrt( vDistance.m128_f32[0] + vDistance.m128_f32[1] + vDistance.m128_f32[2] );

		////

		XMVECTOR velocity = linearMovingVector( mTargetPoint ) * fDistance /*mObservableObject->GetScaleCoeff()*/ * 0.5f * mMovementRate * elapsedTime;

		position += velocity;

		MoveForewardViewVector( gameTime );

		XMStoreFloat3( &mPosition, position );

		Camera::Update( gameTime );
	}

	XMVECTOR ThirdPersonCamera::linearMovingVector( XMFLOAT3 mTargetPoint )
	{
		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMVECTOR targetPoint = XMLoadFloat3( &mTargetPoint );
		XMVECTOR vDistance = targetPoint - position;
		vDistance = XMVector3Dot( vDistance, vDistance );
		float fDistance = sqrt( vDistance.m128_f32[0] + vDistance.m128_f32[1] + vDistance.m128_f32[2] );

		if( fDistance > 1.0f )
		{
			return XMVector3Normalize( targetPoint - position );
		}
		else return XMLoadFloat3( &Vector3Helper::Zero );
	}

	void ThirdPersonCamera::radialMovingTo( float targetPolarAngle )
	{

	}

	void ThirdPersonCamera::RadialRotation( float polarAngle, float azimuthAngle )
	{
		XMVECTOR viewVector = XMLoadFloat3( &mViewVector );

		XMMATRIX rotationAroundX = XMMatrixRotationX( polarAngle );
		XMMATRIX rotationAroundY = XMMatrixRotationY( azimuthAngle );

		XMVECTOR temp = XMVector3Transform( viewVector, rotationAroundX );
		XMVECTOR temp2 = XMVector3Transform( temp, rotationAroundY );

		ApplyRotation( XMMatrixMultiply( rotationAroundX, rotationAroundY ) );

		XMStoreFloat3( &mViewVector, temp2 );
	}

	void ThirdPersonCamera::RadialRotation( float polarAngle, float azimuthAngle, XMFLOAT3 mPivotPoint )
	{
		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMVECTOR pivotPoint = XMLoadFloat3( &mPivotPoint );

		XMMATRIX rotationAroundX = XMMatrixRotationX( polarAngle );
		XMMATRIX rotationAroundY = XMMatrixRotationY( azimuthAngle );

		XMVECTOR temp = position - pivotPoint;

		XMVECTOR temp2 = XMVector3Transform( temp, rotationAroundX );
		XMVECTOR temp3 = XMVector3Transform( temp2, rotationAroundY );

		position = temp3 + pivotPoint;//return to global coord system

		ApplyRotation( XMMatrixMultiply( rotationAroundX, rotationAroundY ) );

		XMStoreFloat3( &mPosition, position );
	}

	void ThirdPersonCamera::MouseRotation()
	{
		//Free rotation around camera position controlled by mouse
		XMFLOAT2 rotationAmount = Vector2Helper::Zero;
		if( (mMouse != nullptr) && (mMouse->IsButtonHeldDown( MouseButtonsLeft )) )
		{
			LPDIMOUSESTATE mouseState = mMouse->CurrentState();
			rotationAmount.x = -mouseState->lX * mMouseSensitivity;
			rotationAmount.y = -mouseState->lY * mMouseSensitivity;
		}
		//end of free rotational by mouse
	}

	void ThirdPersonCamera::MoveForewardViewVector( const GameTime& gameTime )
	{
		float elapsedTime = (float)gameTime.ElapsedGameTime();
		XMVECTOR viewVector = XMLoadFloat3( &mViewVector );
		XMFLOAT2 movementAmount = Vector2Helper::Zero;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->IsKeyDown( DIK_W ) )
			{
				movementAmount.y = 1.0f;
			}

			if( mKeyboard->IsKeyDown( DIK_S ) )
			{
				movementAmount.y = -1.0f;
			}

			//if (mKeyboard->IsKeyDown(DIK_A))
			//{
			//	movementAmount.x = -1.0f;
			//}

			//if (mKeyboard->IsKeyDown(DIK_D))
			//{
			//	movementAmount.x = 1.0f;
			//}
		}

		if( (mMouse != nullptr) )
		{
			if( mMouse->IsWheelTurning() )
			{
				LPDIMOUSESTATE mouseStateCur = mMouse->CurrentState();
				LPDIMOUSESTATE mouseStatePrev = mMouse->LastState();

				if( ((int)(mouseStateCur->lZ) > (int)(mouseStatePrev->lZ)) )
				{
					movementAmount.y = 100.0f;
				}
				else
				{
					movementAmount.y = -100.0f;
				}
			}
		}

		XMVECTOR movement = XMLoadFloat2( &movementAmount )* mMovementRate * elapsedTime;

		//XMVECTOR strafe = right * XMVectorGetX(movement);
		//position += strafe;

		XMVECTOR forward = XMLoadFloat3( &mDirection ) * XMVectorGetY( movement ) * 10.0f;
		viewVector += forward;

		XMStoreFloat3( &mViewVector, viewVector );

	}

	CameraViewPosition ThirdPersonCamera::GetCameraViewPosition() const
	{
		return mCameraViewPosition;
	}

	void ThirdPersonCamera::SetObservableObject( ObservableObject* object )
	{
		mObservableObject = object;
	}

	std::string ThirdPersonCamera::DrawCameraViewPosString() const
	{
		return DrawCameraViewPosNames[(int)mCameraViewPosition];
	}

}
