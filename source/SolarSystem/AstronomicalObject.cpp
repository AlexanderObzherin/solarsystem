#include "AstronomicalObject.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"

#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"

#include "Keyboard.h"

#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( AstronomicalObject )

	AstronomicalObject::AstronomicalObject( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis )
		:
		ObservableObject( game, camera ),
		mOrbitAxis( orbitAxis ),

		mScaleMatrix( MatrixHelper::Identity ),
		mWorldMatrix( MatrixHelper::Identity ),

		mAxisRotationAngle( 0.0f ),
		mAxisRotationAngleVelocityCoeff( 0.0f ),
		mOrbita( 0.0f ),
		mOrbitRotationAngle( 0.0f ),
		mOrbitRotationAngleVelocityCoeff( 0.0f )
	{
		XMStoreFloat4x4( &mScaleMatrix, XMMatrixScaling( scale, scale, scale ) );
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	AstronomicalObject::~AstronomicalObject()
	{
	}

	void AstronomicalObject::Update( const GameTime& gameTime )
	{
		// set distance between planet and orbita axis
		XMFLOAT3 distance = XMFLOAT3( mOrbita, 0.0f, 0.0f );

		// update rotation angles
		mAxisRotationAngle += XM_PI * static_cast<float>(gameTime.ElapsedGameTime() * mAxisRotationAngleVelocityCoeff);
		mOrbitRotationAngle += XM_PI * static_cast<float>(gameTime.ElapsedGameTime() * mOrbitRotationAngleVelocityCoeff);

		//set rotation matrices
		XMMATRIX axisRotation = XMMatrixRotationY( mAxisRotationAngle );
		XMMATRIX orbitRotation = XMMatrixRotationY( mOrbitRotationAngle );

		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMVECTOR orbitAxis = XMLoadFloat3( &mOrbitAxis );

		//// create and set transition matrix to follow the orbita axis

		XMMATRIX toCurSystem = XMMatrixTranslationFromVector( orbitAxis );

		XMMATRIX worldMatrix = XMMatrixIdentity();
		XMMATRIX scaleMatrix = XMLoadFloat4x4( &mScaleMatrix );

		MatrixHelper::SetTranslation( worldMatrix, distance );

		//W = Raxial * T * Rorbit
		XMStoreFloat4x4( &mWorldMatrix, scaleMatrix * axisRotation * worldMatrix * orbitRotation * toCurSystem );

		//Update mPosition after concatenated matrix transformations
		XMMATRIX tempmatrix = XMLoadFloat4x4( &mWorldMatrix );
		XMVECTOR tempvector = XMLoadFloat3( &Vector3Helper::One );
		XMVECTOR tempvector2 = XMLoadFloat3( &Vector3Helper::One );
		tempvector2 = XMVector3Transform( tempvector, tempmatrix );
		
		XMVECTOR averVelocity = position - tempvector2;
		XMStoreFloat3( &mAverVelocity, averVelocity );
		XMStoreFloat3( &mPosition, tempvector2 );
	}

	void AstronomicalObject::SetObjectCharacteristics()
	{
			//Here we can set individual kinematical characteristics and object's description
			//mAxisRotationAngle = 0.0f;
			//mAxisRotationAngleVelocityCoeff = 0.0f;
			//mOrbita = 0.0f;
			//mOrbitRotationAngle = 0.0f;
			//mOrbitRotationAngleVelocityCoeff = 0.0f;
	}

	void AstronomicalObject::SetObjectTexture()
	{
	}

	float AstronomicalObject::GetScaleCoeff()
	{
		return scaleCoefficient;
	}

	std::vector<std::string> AstronomicalObject::ObjectDesc()
	{
		return std::vector<std::string>{};
	}
}