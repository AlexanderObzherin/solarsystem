#pragma once

#include "ObservableObject.h"

using namespace Library;

namespace Library
{
	class Keyboard;
}

namespace Rendering
{
	/*! This class in the astronomical object hierarchy creates main mechanical behavior like axis and orbital rotatings, etc.
	*/
	class AstronomicalObject : public ObservableObject
	{
		RTTI_DECLARATIONS( AstronomicalObject, ObservableObject )

	public:
		AstronomicalObject( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis );
		~AstronomicalObject();

		virtual void Update( const GameTime& gameTime ) override;

		virtual void SetObjectCharacteristics();
		virtual void SetObjectTexture();
		virtual float GetScaleCoeff() override;
		virtual std::vector<std::string> ObjectDesc();

	protected:
		AstronomicalObject();
		AstronomicalObject( const AstronomicalObject& rhs );
		AstronomicalObject& operator=(const AstronomicalObject& rhs);

		const XMFLOAT3& mOrbitAxis;

		//kinematika
		XMFLOAT4X4 mScaleMatrix;
		XMFLOAT4X4 mWorldMatrix;
		//XMFLOAT4X4 mRotationMatrix;

		float mAxisRotationAngle;
		float mAxisRotationAngleVelocityCoeff;

		float mOrbitRotationAngle;
		float mOrbitRotationAngleVelocityCoeff;

		float mOrbita;

	};
}