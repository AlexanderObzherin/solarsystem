#include "Callisto.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Callisto )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Callisto::Callisto( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Callisto::~Callisto()
	{
	}

	void Callisto::Initialize()
	{
		Planet::Initialize();
	}

	void Callisto::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Callisto::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Callisto::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\callisto.png";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Callisto::SetObjectCharacteristics()
	{
		mOrbita = 90.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.03f;

		mOrbitRotationAngle = -XM_PIDIV2;
		mOrbitRotationAngleVelocityCoeff = 0.018f;

		mObjectDescription.push_back( std::string{ "Name: Callisto" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.075938 * 10^23 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 2410.3 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 1.235 m/s^2 (0.126g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 16.6890184 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 16.6890184 d" } );
	}

	std::vector<std::string> Callisto::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Callisto::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}