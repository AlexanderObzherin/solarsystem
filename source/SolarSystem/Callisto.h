#pragma once

#include "Planet.h"

namespace Rendering
{
	class Callisto : public Planet
	{
		RTTI_DECLARATIONS( Callisto, Planet )

	public:
		Callisto( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Callisto();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Callisto();
		Callisto( const Callisto& rhs );
		Callisto& operator=( const Callisto& rhs );
	};
}