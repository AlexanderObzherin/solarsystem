#include "Dione.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Dione )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Dione::Dione( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Dione::~Dione()
	{
	}

	void Dione::Initialize()
	{
		Planet::Initialize();
	}

	void Dione::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Dione::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Dione::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\dione.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Dione::SetObjectCharacteristics()
	{
		mOrbita = 75.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.015f;

		mObjectDescription.push_back( std::string{ "Name: Dione" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.095452 * 10^21 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 561.4 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 0.232 m/s^2" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 2.736915 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 2.736915 d" } );
	}

	std::vector<std::string> Dione::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Dione::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}