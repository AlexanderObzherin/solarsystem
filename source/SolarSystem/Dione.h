#pragma once

#include "Planet.h"

namespace Rendering
{
	class Dione : public Planet
	{
		RTTI_DECLARATIONS( Dione, Planet )

	public:
		Dione( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Dione();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Dione();
		Dione( const Dione& rhs );
		Dione& operator=( const Dione& rhs );
	};
}