#include "Earth.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include <DDSTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Earth )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Earth::Earth( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		SetObjectCharacteristics();
	}

	Earth::~Earth()
	{
	}

	void Earth::Initialize()
	{
		Planet::Initialize();
	}

	void Earth::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Earth::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Earth::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\Earthatday.dds";
		HRESULT hr = DirectX::CreateDDSTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateWICTextureFromFile() failed.", hr);
		}
	}

	void Earth::SetObjectCharacteristics()
	{
		mOrbita = 120.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.05f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.015f;

		mObjectDescription.push_back(std::string{ "Name: Earth" });
		mObjectDescription.push_back(std::string{ "Mass: 5.97237 * 10^24 kg" });
		mObjectDescription.push_back(std::string{ "Radius: 6371.0 km" });
		mObjectDescription.push_back(std::string{ "Surface gravity: 9.807 m/s^2" });
		mObjectDescription.push_back(std::string{ "Orbital period: 365.256 d" });
		mObjectDescription.push_back(std::string{ "Sidereal rotation period: 0.99726968 d" });
	}

	std::vector<std::string> Earth::ObjectDesc()
	{
		return mObjectDescription;
	}
}