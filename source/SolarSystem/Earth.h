#pragma once

#include "Planet.h"

namespace Rendering
{
	class Earth : public Planet
	{
		RTTI_DECLARATIONS( Earth, Planet )

	public:
		Earth( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Earth();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string>ObjectDesc();

	private:
		Earth();
		Earth( const Earth& rhs );
		Earth& operator=(const Earth& rhs);
	};
}