#include "Enceladus.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Enceladus )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Enceladus::Enceladus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Enceladus::~Enceladus()
	{
	}

	void Enceladus::Initialize()
	{
		Planet::Initialize();
	}

	void Enceladus::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Enceladus::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Enceladus::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\enceladus.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Enceladus::SetObjectCharacteristics()
	{
		mOrbita = 50.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.025f;

		mObjectDescription.push_back( std::string{ "Name: Enceladus" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.08022 * 10^20 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 252.1 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 0.113 m/s^2 (0.0113g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 1.370218 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 1.370218 d" } );
	}

	std::vector<std::string> Enceladus::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Enceladus::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}