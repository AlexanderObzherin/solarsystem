#pragma once

#include "Planet.h"

namespace Rendering
{
	class Enceladus : public Planet
	{
		RTTI_DECLARATIONS( Enceladus, Planet )

	public:
		Enceladus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Enceladus();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Enceladus();
		Enceladus( const Enceladus& rhs );
		Enceladus& operator=( const Enceladus& rhs );
	};
}