#include "Europa.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Europa )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Europa::Europa( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Europa::~Europa()
	{
	}

	void Europa::Initialize()
	{
		Planet::Initialize();
	}

	void Europa::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Europa::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Europa::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\europa.png";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Europa::SetObjectCharacteristics()
	{
		mOrbita = 50.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.03f;

		mOrbitRotationAngle = XM_PIDIV2;
		mOrbitRotationAngleVelocityCoeff = 0.025f;

		mObjectDescription.push_back( std::string{ "Name: Europa" } );
		mObjectDescription.push_back( std::string{ "Mass: 4.799844 * 10^22 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 1560.8 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 1.314 m/s^2 (0.134g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 3.551181 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 3.551181 d" } );
	}

	std::vector<std::string> Europa::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Europa::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}