#pragma once

#include "Planet.h"

namespace Rendering
{
	class Europa : public Planet
	{
		RTTI_DECLARATIONS( Europa, Planet )

	public:
		Europa( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Europa();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Europa();
		Europa( const Europa& rhs );
		Europa& operator=( const Europa& rhs );
	};
}