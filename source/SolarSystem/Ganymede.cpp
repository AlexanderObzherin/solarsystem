#include "Ganymede.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Ganymede )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Ganymede::Ganymede( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Ganymede::~Ganymede()
	{
	}

	void Ganymede::Initialize()
	{
		Planet::Initialize();
	}

	void Ganymede::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Ganymede::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Ganymede::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\ganymede.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Ganymede::SetObjectCharacteristics()
	{
		mOrbita = 70.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.03f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.02f;

		mObjectDescription.push_back( std::string{ "Name: Ganymede" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.4819 * 10^23 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 2634.1 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 1.428 m/s^2 (0.146g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 7.154552 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 7.154552 d" } );
	}

	std::vector<std::string> Ganymede::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Ganymede::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}