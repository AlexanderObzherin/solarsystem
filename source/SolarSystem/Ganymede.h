#pragma once

#include "Planet.h"

namespace Rendering
{
	class Ganymede : public Planet
	{
		RTTI_DECLARATIONS( Ganymede, Planet )

	public:
		Ganymede( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Ganymede();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Ganymede();
		Ganymede( const Ganymede& rhs );
		Ganymede& operator=( const Ganymede& rhs );
	};
}