#include "Iapetus.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Iapetus )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Iapetus::Iapetus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Iapetus::~Iapetus()
	{
	}

	void Iapetus::Initialize()
	{
		Planet::Initialize();
	}

	void Iapetus::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Iapetus::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Iapetus::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\iapetus.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Iapetus::SetObjectCharacteristics()
	{
		mOrbita = 135.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.007f;

		mObjectDescription.push_back( std::string{ "Name: Iapetus" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.805635 * 10^21 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 734.5 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 0.223 m/s^2" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 79.3215 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 79.3215 d" } );
	}

	std::vector<std::string> Iapetus::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Iapetus::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}