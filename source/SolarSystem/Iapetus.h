#pragma once

#include "Planet.h"

namespace Rendering
{
	class Iapetus : public Planet
	{
		RTTI_DECLARATIONS( Iapetus, Planet )

	public:
		Iapetus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Iapetus();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Iapetus();
		Iapetus( const Iapetus& rhs );
		Iapetus& operator=( const Iapetus& rhs );
	};
}
