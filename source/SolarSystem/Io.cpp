#include "Io.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Io )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Io::Io( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Io::~Io()
	{
	}

	void Io::Initialize()
	{
		Planet::Initialize();
	}

	void Io::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Io::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Io::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\io.png";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Io::SetObjectCharacteristics()
	{
		mOrbita = 35.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.03f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.03f;

		mObjectDescription.push_back( std::string{ "Name: Io" } );
		mObjectDescription.push_back( std::string{ "Mass: 8.931938 * 10^22 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 1821.6 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 1.796 m/s^2 (0.183g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 42.45930686 h" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 42.45930686 h" } );
	}

	std::vector<std::string> Io::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Io::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}