#include "Jupiter.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Jupiter )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Jupiter::Jupiter( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Jupiter::~Jupiter()
	{
	}

	void Jupiter::Initialize()
	{
		Planet::Initialize();
	}

	void Jupiter::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Jupiter::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Jupiter::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\jupiter.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile(mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateWICTextureFromFile() failed.", hr);
		}
	}

	void Jupiter::SetObjectCharacteristics()
	{
		mOrbita = 550.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.005f;

		mObjectDescription.push_back(std::string{ "Name: Jupiter" });
		mObjectDescription.push_back(std::string{ "Mass: 1.8986 * 10^27 kg" });
		mObjectDescription.push_back(std::string{ "Radius: 69911 km" });
		mObjectDescription.push_back(std::string{ "Surface gravity: 24.79 m/s^2 (2.528g)" });
		mObjectDescription.push_back(std::string{ "Orbital period: 11.8618 d" });
		mObjectDescription.push_back(std::string{ "Sidereal rotation period: 9.925 h" });
	}

	std::vector<std::string> Jupiter::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Jupiter::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}