#pragma once

#include "Planet.h"

namespace Rendering
{
	class Jupiter : public Planet
	{
		RTTI_DECLARATIONS( Jupiter, Planet )

	public:
		Jupiter( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Jupiter();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Jupiter();
		Jupiter( const Jupiter& rhs );
		Jupiter& operator=( const Jupiter& rhs );
	};
}
