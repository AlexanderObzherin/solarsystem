#include "Mars.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Mars )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

	Mars::Mars( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Mars::~Mars()
	{
	}

	void Mars::Initialize()
	{
		Planet::Initialize();
	}

	void Mars::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Mars::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Mars::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\mars.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile(mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateWICTextureFromFile() failed.", hr);
		}
	}

	void Mars::SetObjectCharacteristics()
	{
		mOrbita = 160.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.05f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.01f;

		mObjectDescription.push_back(std::string{ "Name: Mars" });
		mObjectDescription.push_back(std::string{ "Mass: 6.4171 * 10^23 kg" });
		mObjectDescription.push_back(std::string{ "Radius: 3389.5 km" });
		mObjectDescription.push_back(std::string{ "Surface gravity: 3.711 m/s^2 (0.376g)" });
		mObjectDescription.push_back(std::string{ "Orbital period: 686.971 d" });
		mObjectDescription.push_back(std::string{ "Sidereal rotation period: 1.025957 d" });
	}

	std::vector<std::string> Mars::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Mars::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}