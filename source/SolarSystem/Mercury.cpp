#include "Mercury.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include <DDSTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Mercury )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

	Mercury::Mercury( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		SetObjectCharacteristics();
	}

	Mercury::~Mercury()
	{
	}

	void Mercury::Initialize()
	{
		Planet::Initialize();
	}

	void Mercury::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Mercury::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Mercury::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\mercury.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile(mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateWICTextureFromFile() failed.", hr);
		}
	}

	void Mercury::SetObjectCharacteristics()
	{
		mOrbita = 60.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.005f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.04f;

		mObjectDescription.push_back(std::string{ "Name: Mercury" });
		mObjectDescription.push_back(std::string{ "Mass: 3.3011 * 10^23 kg" });
		mObjectDescription.push_back(std::string{ "Radius: 2439.7 km" });
		mObjectDescription.push_back(std::string{ "Surface gravity: 3.7 m/s^2 (0.38g)" });
		mObjectDescription.push_back(std::string{ "Orbital period: 87.969 d" });
		mObjectDescription.push_back(std::string{ "Sidereal rotation period: 58646 d" });
	}

	std::vector<std::string> Mercury::ObjectDesc()
	{
		return mObjectDescription;
	}
}