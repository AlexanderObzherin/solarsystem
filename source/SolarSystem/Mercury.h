#pragma once

#include "Planet.h"

namespace Rendering
{
	class Mercury : public Planet
	{
		RTTI_DECLARATIONS( Mercury, Planet )

	public:
		Mercury( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Mercury();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string>ObjectDesc();

	private:
		Mercury();
		Mercury( const Mercury& rhs );
		Mercury& operator=(const Mercury& rhs);
	};
}