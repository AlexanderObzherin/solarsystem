#include "Mimas.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Mimas )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Mimas::Mimas( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Mimas::~Mimas()
	{
	}

	void Mimas::Initialize()
	{
		Planet::Initialize();
	}

	void Mimas::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Mimas::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Mimas::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\mimas.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Mimas::SetObjectCharacteristics()
	{
		mOrbita = 40.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.03f;

		mObjectDescription.push_back( std::string{ "Name: Mimas" } );
		mObjectDescription.push_back( std::string{ "Mass: 3.7493 * 10^19 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 198.2 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 0.064 m/s^2 (0.00648g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 0.942 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 0.942 d" } );
	}

	std::vector<std::string> Mimas::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Mimas::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}