#pragma once

#include "Planet.h"

namespace Rendering
{
	class Mimas : public Planet
	{
		RTTI_DECLARATIONS( Mimas, Planet )

	public:
		Mimas( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Mimas();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Mimas();
		Mimas( const Mimas& rhs );
		Mimas& operator=( const Mimas& rhs );
	};
}