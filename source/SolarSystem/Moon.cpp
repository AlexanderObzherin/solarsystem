#include "Moon.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include <DDSTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Moon )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Moon::Moon( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Moon::~Moon()
	{
	}

	void Moon::Initialize()
	{
		Planet::Initialize();
	}

	void Moon::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Moon::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Moon::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\moon.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile(mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateWICTextureFromFile() failed.", hr);
		}
	}

	void Moon::SetObjectCharacteristics()
	{
		mOrbita = 18.0f;
		mAxisRotationAngle = 10.0f;
		mOrbitRotationAngleVelocityCoeff = 0.02;

		mObjectDescription.push_back(std::string{ "Name: Moon" });
		mObjectDescription.push_back(std::string{ "Mass: 7.342 * 10^22 kg" });
		mObjectDescription.push_back(std::string{ "Radius: 1737.1 km" });
		mObjectDescription.push_back(std::string{ "Surface gravity: 1.62 m/s^2 (0.1654g)" });
		mObjectDescription.push_back(std::string{ "Orbital period: 27.321661 d" });
		mObjectDescription.push_back(std::string{ "Sidereal rotation period: 27.321661 d d" });
	}

	float Moon::GetScaleCoeff()
	{
		return scaleCoefficient;
	}

	std::vector<std::string> Moon::ObjectDesc()
	{
		return mObjectDescription;
	}
}