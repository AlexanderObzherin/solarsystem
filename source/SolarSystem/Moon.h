#pragma once

#include "Planet.h"

namespace Rendering
{
	class Moon : public Planet
	{
		RTTI_DECLARATIONS( Moon, Planet )

	public:
		Moon( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Moon();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual float GetScaleCoeff();
		virtual std::vector<std::string>ObjectDesc();
	private:
		Moon();
		Moon( const Moon& rhs );
		Moon& operator=(const Moon& rhs);
	};
}