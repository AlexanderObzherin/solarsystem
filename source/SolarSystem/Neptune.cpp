#include "Neptune.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Neptune )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Neptune::Neptune( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Neptune::~Neptune()
	{
	}

	void Neptune::Initialize()
	{
		Planet::Initialize();
	}

	void Neptune::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Neptune::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Neptune::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\neptune.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Neptune::SetObjectCharacteristics()
	{
		mOrbita = 1200.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.005f;

		mObjectDescription.push_back( std::string{ "Name: Neptune" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.0243 * 10^26 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 24622 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 11.15 m/s^2 (1.14g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 164.8 yr" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 0.6713 d" } );
	}

	std::vector<std::string> Neptune::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Neptune::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}