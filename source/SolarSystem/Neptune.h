#pragma once

#include "Planet.h"

namespace Rendering
{
	class Neptune : public Planet
	{
		RTTI_DECLARATIONS( Neptune, Planet )

	public:
		Neptune( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Neptune();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Neptune();
		Neptune( const Neptune& rhs );
		Neptune& operator=( const Neptune& rhs );
	};
}
