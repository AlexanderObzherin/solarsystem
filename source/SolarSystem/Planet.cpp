#include "Planet.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include <DDSTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Planet )

	const float Planet::LightModulationRate = UCHAR_MAX;
	const float Planet::LightMovementRate = 10.0f;

	Planet::Planet( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		AstronomicalObject( game, camera, scale, orbitAxis ),
		mEffect( nullptr ),
		mMaterial( nullptr ),
		mTextureShaderResourceView( nullptr ),
		mVertexBuffer( nullptr ),
		mIndexBuffer( nullptr ),
		mIndexCount( 0 ),
		mKeyboard( nullptr ),
		mAmbientColor( 1, 1, 1, 0.08f ),

		mPointLightSource( mPointLightSource ),
		mSpecularColor( 1.0f, 1.0f, 1.0f, 1.0f ),
		mSpecularPower( 25.0f )

	{
		//XMStoreFloat4x4( &mScaleMatrix, XMMatrixScaling( scale, scale, scale ) );
	}

	Planet::~Planet()
	{
		ReleaseObject( mTextureShaderResourceView );
		DeleteObject( mMaterial );
		DeleteObject( mEffect );
		ReleaseObject( mVertexBuffer );
		ReleaseObject( mIndexBuffer );
	}

	void Planet::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		std::unique_ptr<Model> model( new Model( *mGame, "Content\\Models\\Sphere.obj", true ) );

		// Initialize the material
		mEffect = new Effect( *mGame );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\PointLight.cso" );

		mMaterial = new PointLightMaterial();
		mMaterial->Initialize( mEffect );

		Mesh* mesh = model->Meshes().at( 0 );
		mMaterial->CreateVertexBuffer( mGame->Direct3DDevice(), *mesh, &mVertexBuffer );
		mesh->CreateIndexBuffer( &mIndexBuffer );
		mIndexCount = mesh->Indices().size();

		SetObjectTexture();
		
		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		assert( mKeyboard != nullptr );
	}

	void Planet::Update( const GameTime& gameTime )
	{
		UpdateAmbientLight( gameTime );

		AstronomicalObject::Update( gameTime );
	}

	void Planet::Draw( const GameTime& gameTime )
	{
		ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();
		direct3DDeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		Pass* pass = mMaterial->CurrentTechnique()->Passes().at( 0 );
		ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at( pass );
		direct3DDeviceContext->IASetInputLayout( inputLayout );

		UINT stride = mMaterial->VertexSize();
		UINT offset = 0;
		direct3DDeviceContext->IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );
		direct3DDeviceContext->IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

		XMMATRIX worldMatrix = XMLoadFloat4x4( &mWorldMatrix );
		XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();
		XMVECTOR ambientColor = XMLoadColor( &mAmbientColor );
		XMVECTOR specularColor = XMLoadColor( &mSpecularColor );

		mMaterial->WorldViewProjection() << wvp;
		mMaterial->World() << worldMatrix;
		mMaterial->SpecularColor() << specularColor;
		mMaterial->SpecularPower() << mSpecularPower;
		mMaterial->AmbientColor() << ambientColor;
		//mMaterial->LightColor() << mPointLight->ColorVector();
		//mMaterial->LightPosition() << mPointLight->PositionVector();
		//mMaterial->LightRadius() << mPointLight->Radius();
		mMaterial->LightColor() << mPointLightSource.ColorVector();
		mMaterial->LightPosition() << mPointLightSource.PositionVector();
		mMaterial->LightRadius() << mPointLightSource.Radius();


		mMaterial->ColorTexture() << mTextureShaderResourceView;
		mMaterial->CameraPosition() << mCamera->PositionVector();

		pass->Apply( 0, direct3DDeviceContext );

		direct3DDeviceContext->DrawIndexed( mIndexCount, 0, 0 );

	}

	void Planet::UpdateSpecularLight( const GameTime& gameTime )
	{
		static float specularIntensity = mSpecularPower;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->IsKeyDown( DIK_INSERT ) && specularIntensity < UCHAR_MAX )
			{
				specularIntensity += LightModulationRate * (float)gameTime.ElapsedGameTime();
				specularIntensity = XMMin<float>( specularIntensity, UCHAR_MAX );

				mSpecularPower = specularIntensity;;
			}

			if( mKeyboard->IsKeyDown( DIK_DELETE ) && specularIntensity > 0 )
			{
				specularIntensity -= LightModulationRate * (float)gameTime.ElapsedGameTime();
				specularIntensity = XMMax<float>( specularIntensity, 0 );

				mSpecularPower = specularIntensity;
			}
		}
	}

	void Planet::UpdateAmbientLight( const GameTime& gameTime )
	{
		static float ambientIntensity = mAmbientColor.a;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->IsKeyDown( DIK_PGUP ) && ambientIntensity < UCHAR_MAX )
			{
				ambientIntensity += LightModulationRate * (float)gameTime.ElapsedGameTime();
				mAmbientColor.a = (UCHAR)XMMin<float>( ambientIntensity, UCHAR_MAX );
			}

			if( mKeyboard->IsKeyDown( DIK_PGDN ) && ambientIntensity > 0 )
			{
				ambientIntensity -= LightModulationRate * (float)gameTime.ElapsedGameTime();
				mAmbientColor.a = (UCHAR)XMMax<float>( ambientIntensity, 0 );
			}
		}
	}

	std::vector<std::string> Planet::ObjectDesc()
	{
		return std::vector<std::string>{ "Type : Planet" };
	}

	void Planet::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\Earthatday.dds";
		HRESULT hr = DirectX::CreateDDSTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateDDSTextureFromFile() failed.", hr);
		}
	}

	float Planet::GetScaleCoeff()
	{
		return AstronomicalObject::GetScaleCoeff();
	}
}