#pragma once

#include "AstronomicalObject.h"

using namespace Library;

namespace Library
{
	class Effect;
	class PointLight;
	class Keyboard;
}

namespace Rendering
{
	class PointLightMaterial;

	/*! This class in the astronomical object hierarchy creates shading properties of the planets, moons, etc.,
	in particular shading considering point light source.
	*/
	class Planet : public AstronomicalObject
	{
		RTTI_DECLARATIONS( Planet, AstronomicalObject )

	public:
		Planet( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Planet();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		//const XMFLOAT3& Position() const;
		virtual float GetScaleCoeff();

		virtual std::vector<std::string> ObjectDesc();
		virtual void SetObjectTexture() override;

	protected:
		Planet();
		Planet( const Planet& rhs );
		Planet& operator=(const Planet& rhs);

		void UpdateAmbientLight( const GameTime& gameTime );
		void UpdatePointLight( const GameTime& gameTime );
		void UpdateSpecularLight( const GameTime& gameTime );

		static const float LightModulationRate;
		static const float LightMovementRate;

		Effect* mEffect;
		PointLightMaterial* mMaterial;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		Keyboard* mKeyboard;
		XMCOLOR mAmbientColor;

		const PointLight& mPointLightSource;

		XMCOLOR mSpecularColor;
		float mSpecularPower;
	};
}