/*! \mainpage
3D demo scene with the solar system object's visualization.
This 3d Rendering application was made using DirectX API. Demo scene doesn't modeling real mechanics of the astronomical objects, 
but rude approximations of the planets and moons movements.
<br>
Textures of planets downloaded from:
<br>
http://planetpixelemporium.com/planets.html
<br>
http://www.celestiamotherlode.net/
<br>
3D models imported with Assimp parser (www.assimp.org) Astronomical objects can be observed with virtual camera which follows the objects
and rotating around by the polar angle changing. Angle can be changed to one of the five fixed positions 
(top, angle 45 deg, side, -45 deg and bottom view) by Q/A buttons, distance can be changed by W/S buttons. 
Observable object can be changed with E/D buttons. In the right area of screen brief description of the current object is displayed. 
This demo is represents the Sun in the center of scene and rounding planets with their main natural satellites. For shading planets and moons
there is point light source is used, placed in the center of the star. For entire scene, as a post-process effect, the bloom effect is added.
*/


#include <memory>
#include "GameException.h"
#include "RenderingGame.h"

#if defined(DEBUG) || defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

using namespace Library;
using namespace Rendering;

int WINAPI WinMain( HINSTANCE instance, HINSTANCE previousInstance, LPSTR commandLine, int showCommand )
{
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	std::unique_ptr<RenderingGame> game( new RenderingGame( instance, L"RenderingClass", L"Real-Time 3D Rendering", showCommand ) );

	try
	{
		game->Run();
	}
	catch( GameException ex )
	{
		MessageBox( game->WindowHandle(), ex.whatw().c_str(), game->WindowTitle().c_str(), MB_ABORTRETRYIGNORE );
	}

	return 0;
}