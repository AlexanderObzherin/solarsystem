#include "RenderingGame.h"
#include "GameException.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "FpsComponent.h"
#include "VectorHelper.h"
#include "ColorHelper.h"
#include "ThirdPersonCamera.h"
#include "RenderStateHelper.h"
#include "RasterizerStates.h"
#include "SamplerStates.h"
#include "Grid.h"
#include "Skybox.h"

#include "FullScreenRenderTarget.h"
#include "Bloom.h"

#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>

#include "Star.h"
#include "Sun.h"
#include "Mercury.h"
#include "Venus.h"
#include "Earth.h"
#include "Moon.h"
#include "Mars.h"

#include "Jupiter.h"
#include "Io.h"
#include "Europa.h"
#include "Ganymede.h"
#include "Callisto.h"

#include "Saturn.h"
#include "RingsOfSaturn.h"
#include "Mimas.h"
#include "Enceladus.h"
#include "Tethys.h"
#include "Dione.h"
#include "Rhea.h"
#include "Titan.h"
#include "Iapetus.h"

#include "Uranus.h"
#include "Neptune.h"

namespace Rendering
{
	const float RenderingGame::BloomModulationRate = 1.0f;
	const XMVECTORF32 RenderingGame::BackgroundColor = { 0.0f, 0.0f, 0.0f, 1.0f };//black

	RenderingGame::RenderingGame( HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand )
		:
		Game( instance, windowClass, windowTitle, showCommand ),
		mFpsComponent( nullptr ),
		mDirectInput( nullptr ),
		mKeyboard( nullptr ),
		mMouse( nullptr ),
		mRenderStateHelper( nullptr ),
		mGrid( nullptr ),
		mSkybox( nullptr ),

		mFullScreenRenderTarget( nullptr ),
		mBloom( nullptr ),
		mBloomEnabled( true ),

		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr ),
		mTextPositionLeftSide( 0.0f, 40.0f ),
		mTextPositionRightSide( 840.0f, 40.0f ),

		mAstronomicalObject( nullptr ),

		mStar( nullptr )

	{
		mDepthStencilBufferEnabled = true;
		mMultiSamplingEnabled = true;
	}

	RenderingGame::~RenderingGame()
	{
	}

	void RenderingGame::Initialize()
	{
		if( FAILED( DirectInput8Create( mInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&mDirectInput, nullptr ) ) )
		{
			throw GameException( "DirectInput8Create() failed" );
		}
		
		mKeyboard = new Keyboard( *this, mDirectInput );
		mComponents.push_back( mKeyboard );
		mServices.AddService( Keyboard::TypeIdClass(), mKeyboard );

		mMouse = new Mouse( *this, mDirectInput );
		mComponents.push_back( mMouse );
		mServices.AddService( Mouse::TypeIdClass(), mMouse );

		mCamera = new ThirdPersonCamera( *this );
		mComponents.push_back( mCamera );
		mServices.AddService( Camera::TypeIdClass(), mCamera );

		mFpsComponent = new FpsComponent( *this );
		mFpsComponent->Initialize();

		mSkybox = new Skybox( *this, *mCamera, L"Content\\Textures\\MilkywaySkybox.dds", 150.0f );
		mComponents.push_back( mSkybox );

		mSpriteBatch = new SpriteBatch( mDirect3DDeviceContext );
		mSpriteFont = new SpriteFont( mDirect3DDevice, L"Content\\Fonts\\TimesNewRoman_12_bold.spritefont" );

		RasterizerStates::Initialize( mDirect3DDevice );
		SamplerStates::BorderColor = ColorHelper::Black;
		SamplerStates::Initialize( mDirect3DDevice );

		//We need to initialize the Star first, because it consist of PointLight and orbit axis for other planets 
		//Initialize Sun
		solarSystem.push_back( new Sun(*this, *mCamera, 5.0f, Vector3Helper::Zero) );
		mComponents.push_back( solarSystem.back() );
		mStar = dynamic_cast<Star*>( solarSystem.back() );

		//Initialize Mercury
		solarSystem.push_back( new Mercury( *this, *mCamera, 0.2f,  mStar->Position() , *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Venus
		solarSystem.push_back( new Venus( *this, *mCamera, 0.3f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Earth
		solarSystem.push_back( new Earth( *this, *mCamera, 0.3f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );
		auto earth = solarSystem.back();
		
		//Initialize Moon
		solarSystem.push_back( new Moon( *this, *mCamera, 0.05f, earth->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );
		
		//Initialize Mars
		solarSystem.push_back( new Mars( *this, *mCamera, 0.15f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Jupiter
		solarSystem.push_back( new Jupiter( *this, *mCamera, 3.0f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );
		auto jupiter = solarSystem.back();

		//Initialize Io
		solarSystem.push_back( new Io( *this, *mCamera, 0.2f, jupiter->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Europa
		solarSystem.push_back( new Europa( *this, *mCamera, 0.15f, jupiter->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Ganymede
		solarSystem.push_back( new Ganymede( *this, *mCamera, 0.25f, jupiter->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Callisto
		solarSystem.push_back( new Callisto( *this, *mCamera, 0.24f, jupiter->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Saturn
		solarSystem.push_back( new Saturn( *this, *mCamera, 2.5f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );
		auto saturn = solarSystem.back();

		mComponents.push_back( new RingsOfSaturn( *this, *mCamera, 2.5f, mStar->Position(), *(mStar->GetPointLight()) ) );

		//Initialize Mimas
		solarSystem.push_back( new Mimas( *this, *mCamera, 0.1f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Enceladus
		solarSystem.push_back( new Enceladus( *this, *mCamera, 0.1f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Tethys
		solarSystem.push_back( new Tethys( *this, *mCamera, 0.2f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Dione
		solarSystem.push_back( new Dione( *this, *mCamera, 0.2f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Rhea
		solarSystem.push_back( new Rhea( *this, *mCamera, 0.25f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Titan
		solarSystem.push_back( new Titan( *this, *mCamera, 0.5f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Iapetus
		solarSystem.push_back( new Iapetus( *this, *mCamera, 0.25f, saturn->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Uranus
		solarSystem.push_back( new Uranus( *this, *mCamera, 1.2f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );

		//Initialize Neptune
		solarSystem.push_back( new Neptune( *this, *mCamera, 1.2f, mStar->Position(), *(mStar->GetPointLight()) ) );
		mComponents.push_back( solarSystem.back() );
			
		mRenderStateHelper = new RenderStateHelper( *this );

		SolarSystemIterator = solarSystem.begin();

		mCamera->SetObservableObject( *SolarSystemIterator );

		Game::Initialize();

		mFullScreenRenderTarget = new FullScreenRenderTarget( *this );
		mBloom = new Bloom( *this, *mCamera );
		mBloom->SetSceneTexture( *(mFullScreenRenderTarget->OutputTexture()) );
		mBloom->Initialize();
		
		mCamera->SetPosition( solarSystem.front()->Position().x, solarSystem.front()->Position().y, solarSystem.front()->Position().z + 110.0f );
	}

	void RenderingGame::Shutdown()
	{

		DeleteObject( mStar );
		
		DeleteObject( mBloom );
		DeleteObject( mFullScreenRenderTarget );

		DeleteObject( mSkybox );
		DeleteObject( mGrid )
		DeleteObject( mRenderStateHelper );
		DeleteObject( mKeyboard );
		DeleteObject( mMouse );
		DeleteObject( mFpsComponent );
		DeleteObject( mCamera );

		ReleaseObject( mDirectInput );
		RasterizerStates::Release();
		SamplerStates::Release();

		Game::Shutdown();
	}

	void RenderingGame::UpdateBloomSettings( const GameTime & gameTime )
	{
		static BloomSettings bloomSettings = mBloom->GetBloomSettings();

		if (mKeyboard != nullptr)
		{
			if (mKeyboard->IsKeyDown( DIK_J ))
			{
				bloomSettings.BlurAmount += BloomModulationRate * (float)gameTime.ElapsedGameTime();
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_K ) && bloomSettings.BlurAmount > 0.0f)
			{
				bloomSettings.BlurAmount -= BloomModulationRate * (float)gameTime.ElapsedGameTime();
				bloomSettings.BlurAmount = XMMax<float>( bloomSettings.BlurAmount, 0 );
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_U ) && bloomSettings.BloomThreshold < 1.0f)
			{
				bloomSettings.BloomThreshold += BloomModulationRate * (float)gameTime.ElapsedGameTime();
				bloomSettings.BloomThreshold = XMMin<float>( bloomSettings.BloomThreshold, 1.0f );
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_I ) && bloomSettings.BloomThreshold > 0.0f)
			{
				bloomSettings.BloomThreshold -= BloomModulationRate * (float)gameTime.ElapsedGameTime();
				bloomSettings.BloomThreshold = XMMax<float>( bloomSettings.BloomThreshold, 0 );
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_N ))
			{
				bloomSettings.BloomIntensity += BloomModulationRate * (float)gameTime.ElapsedGameTime();
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_M ) && bloomSettings.BloomIntensity > 0.0f)
			{
				bloomSettings.BloomIntensity -= BloomModulationRate * (float)gameTime.ElapsedGameTime();
				bloomSettings.BloomIntensity = XMMax<float>( bloomSettings.BloomIntensity, 0 );
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_G ))
			{
				bloomSettings.BloomSaturation += BloomModulationRate * (float)gameTime.ElapsedGameTime();
				mBloom->SetBloomSettings( bloomSettings );
			}

			if (mKeyboard->IsKeyDown( DIK_H ) && bloomSettings.BloomSaturation > 0.0f)
			{
				bloomSettings.BloomSaturation -= BloomModulationRate * (float)gameTime.ElapsedGameTime();
				bloomSettings.BloomSaturation = XMMax<float>( bloomSettings.BloomSaturation, 0 );
				mBloom->SetBloomSettings( bloomSettings );
			}
		}
	}

	void RenderingGame::Update( const GameTime &gameTime )
	{
		mFpsComponent->Update( gameTime );

		if( mKeyboard->WasKeyPressedThisFrame( DIK_ESCAPE ) )
		{
			Exit();
		}
		
		if( mKeyboard != nullptr )
		{
			if( mKeyboard->WasKeyPressedThisFrame( DIK_E ) )
			{
				if( SolarSystemIterator < solarSystem.end() - 1 )
				{
					SolarSystemIterator++;
				}
			}
			if( mKeyboard->WasKeyPressedThisFrame( DIK_D ) )
			{
				if( SolarSystemIterator > solarSystem.begin() )
				{
					SolarSystemIterator--;
				}
			}

		}
		mCamera->SetObservableObject( *SolarSystemIterator );

		if (mKeyboard->WasKeyPressedThisFrame( DIK_RETURN ))
		{
			BloomDrawMode drawMode = BloomDrawMode( mBloom->DrawMode() + 1 );
			if (drawMode >= BloomDrawModeEnd)
			{
				drawMode = (BloomDrawMode)0;
			}

			mBloom->SetDrawMode( drawMode );
		}

		UpdateBloomSettings( gameTime );

		Game::Update( gameTime );
	}

	void RenderingGame::Draw( const GameTime &gameTime )
	{
		mFullScreenRenderTarget->Begin();

		mDirect3DDeviceContext->ClearRenderTargetView( mFullScreenRenderTarget->RenderTargetView(), reinterpret_cast<const float*>(&BackgroundColor) );
		mDirect3DDeviceContext->ClearDepthStencilView( mFullScreenRenderTarget->DepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0 );

		Game::Draw( gameTime );

		mFullScreenRenderTarget->End();

		mDirect3DDeviceContext->ClearRenderTargetView( mRenderTargetView, reinterpret_cast<const float*>(&BackgroundColor) );
		mDirect3DDeviceContext->ClearDepthStencilView( mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0 );

		mBloom->Draw( gameTime );

		mRenderStateHelper->SaveAll();
		mFpsComponent->Draw( gameTime );

		mSpriteBatch->Begin();
		std::wostringstream helpLabelLeft;
		helpLabelLeft << std::setprecision( 2 ) << "\nCamera View Position (Q / A): " << (mCamera->DrawCameraViewPosString().c_str()) << "\n"
											<< "\nCamera distance (W / S): " "\n";
		mSpriteFont->DrawString( mSpriteBatch, helpLabelLeft.str().c_str(), mTextPositionLeftSide );


		AstronomicalObject* tempObj = *SolarSystemIterator;
		std::wostringstream helpLabelRight;

		helpLabelRight << std::setprecision( 2 ) << "\nObservable object (E / D): " << "\n";
		mSpriteFont->DrawString( mSpriteBatch, helpLabelLeft.str().c_str(), mTextPositionLeftSide );

		ObservableObject* currentObsObj = *SolarSystemIterator;

		int it = 0;
		for( std::string currentString : currentObsObj->ObjectDesc() )
		{
			it++;
			std::wostringstream helpLabel;
			helpLabel << std::setprecision( 2 ) << currentString.c_str() << "\n";
			
			mSpriteFont->DrawString( mSpriteBatch, helpLabel.str().c_str(), XMFLOAT2( 980.0f - (6.0f * (float)(currentString.length())), (80.0f + (float)it * 20.0f )) );
		}


		mSpriteFont->DrawString( mSpriteBatch, helpLabelRight.str().c_str(), mTextPositionRightSide );


		///Bloom Settings
		//std::wostringstream helpLabel;

		//const BloomSettings& bloomSettings = mBloom->GetBloomSettings();
		//helpLabel << std::setprecision( 2 ) << "\nBloom Enabled (Space Bar): " << (mBloomEnabled ? L"True" : L"False") << "\n"
		//	<< L"Draw Mode (Enter): " << mBloom->DrawModeString().c_str() << "\n"
		//	<< L"Bloom Threshold (+U/-I): " << bloomSettings.BloomThreshold << "\n"
		//	<< L"Blur Amount (+J/-K): " << bloomSettings.BlurAmount << "\n"
		//	<< L"Bloom Intensity (+N/-M): " << bloomSettings.BloomIntensity << "\n"
		//	<< L"Bloom Saturation (+G/-H): " << bloomSettings.BloomSaturation << "\n";
		//mSpriteFont->DrawString( mSpriteBatch, helpLabel.str().c_str(), XMFLOAT2( 0.0f, 120.0f ) );
		///

		mSpriteBatch->End();

		mRenderStateHelper->RestoreAll();

		HRESULT hr = mSwapChain->Present( 0, 0 );
		if( FAILED( hr ) )
		{
			throw GameException( "IDXGISwapChain::Present() failed.", hr );
		}
	}
}