#pragma once

#include "Common.h"
#include "Game.h"

using namespace Library;

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Library
{
	class Keyboard;
	class Mouse;
	class ThirdPersonCamera;
	class FpsComponent;
	class RenderStateHelper;
	class Grid;
	class Skybox;

	class FullScreenRenderTarget;
	class Bloom;
}

namespace Rendering
{
	class AstronomicalObject;
	class Star;

	class RenderingGame : public Game
	{
	public:
		RenderingGame( HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand );
		~RenderingGame();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	protected:
		virtual void Shutdown() override;

		std::vector<AstronomicalObject*> solarSystem;
		std::vector<AstronomicalObject*>::iterator SolarSystemIterator = solarSystem.begin();

	private:
		void UpdateBloomSettings( const GameTime& gameTime );

		static const float BloomModulationRate;

		static const XMVECTORF32 BackgroundColor;

		LPDIRECTINPUT8 mDirectInput;
		Keyboard* mKeyboard;
		Mouse* mMouse;
		ThirdPersonCamera* mCamera;
		FpsComponent* mFpsComponent;
		RenderStateHelper* mRenderStateHelper;
		Grid* mGrid;
		Skybox* mSkybox;

		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		XMFLOAT2 mTextPositionLeftSide;
		XMFLOAT2 mTextPositionRightSide;

		AstronomicalObject* mAstronomicalObject;
		Star* mStar;

		FullScreenRenderTarget* mFullScreenRenderTarget;
		Bloom* mBloom;
		bool mBloomEnabled;
	};
}