#include "Rhea.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Rhea )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Rhea::Rhea( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Rhea::~Rhea()
	{
	}

	void Rhea::Initialize()
	{
		Planet::Initialize();
	}

	void Rhea::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Rhea::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Rhea::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\rhea.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Rhea::SetObjectCharacteristics()
	{
		mOrbita = 90.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.013f;

		mObjectDescription.push_back( std::string{ "Name: Rhea" } );
		mObjectDescription.push_back( std::string{ "Mass: 2.306518 * 10^21 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 763.8 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 0.264 m/s^2" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 4.518212 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 4.518212 d" } );
	}

	std::vector<std::string> Rhea::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Rhea::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}