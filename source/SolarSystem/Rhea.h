#pragma once

#include "Planet.h"

namespace Rendering
{
	class Rhea : public Planet
	{
		RTTI_DECLARATIONS( Rhea, Planet )

	public:
		Rhea( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Rhea();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Rhea();
		Rhea( const Rhea& rhs );
		Rhea& operator=( const Rhea& rhs );
	};
}