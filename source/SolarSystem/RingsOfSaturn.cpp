#include "RingsOfSaturn.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( RingsOfSaturn )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		RingsOfSaturn::RingsOfSaturn( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	RingsOfSaturn::~RingsOfSaturn()
	{
	}

	void RingsOfSaturn::Initialize()
	{
		//Planet::Initialize();
		
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		std::unique_ptr<Model> model( new Model( *mGame, "Content\\Models\\RingsOfSaturn.obj", true ) );

		// Initialize the material
		mEffect = new Effect( *mGame );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\PointLight.cso" );

		mMaterial = new PointLightMaterial();
		mMaterial->Initialize( mEffect );

		Mesh* mesh = model->Meshes().at( 0 );
		mMaterial->CreateVertexBuffer( mGame->Direct3DDevice(), *mesh, &mVertexBuffer );
		mesh->CreateIndexBuffer( &mIndexBuffer );
		mIndexCount = mesh->Indices().size();

		SetObjectTexture();

		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		assert( mKeyboard != nullptr );
	}

	void RingsOfSaturn::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void RingsOfSaturn::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void RingsOfSaturn::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\saturnringcolor.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void RingsOfSaturn::SetObjectCharacteristics()
	{
		mOrbita = 650.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.005f;

	}

	std::vector<std::string> RingsOfSaturn::ObjectDesc()
	{
		return mObjectDescription;
	}

	float RingsOfSaturn::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}