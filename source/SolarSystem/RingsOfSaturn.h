#pragma once

#include "Planet.h"

namespace Rendering
{
	class RingsOfSaturn : public Planet
	{
		RTTI_DECLARATIONS( RingsOfSaturn, Planet )

	public:
		RingsOfSaturn( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~RingsOfSaturn();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		RingsOfSaturn();
		RingsOfSaturn( const RingsOfSaturn& rhs );
		RingsOfSaturn& operator=( const RingsOfSaturn& rhs );
	};
}