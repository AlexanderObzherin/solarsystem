#include "Saturn.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Saturn )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Saturn::Saturn( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Saturn::~Saturn()
	{
	}

	void Saturn::Initialize()
	{
		Planet::Initialize();
	}

	void Saturn::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Saturn::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Saturn::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\saturnmap.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Saturn::SetObjectCharacteristics()
	{
		mOrbita = 650.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.005f;

		mObjectDescription.push_back( std::string{ "Name: Saturn" } );
		mObjectDescription.push_back( std::string{ "Mass: 5.6836 * 10^26 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 58232 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 10.44 m/s^2 (1.065g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 29.4571 yr" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 10.55 h" } );
	}

	std::vector<std::string> Saturn::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Saturn::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}