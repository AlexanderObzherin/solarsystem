#pragma once

#include "Planet.h"

namespace Rendering
{
	class Saturn : public Planet
	{
		RTTI_DECLARATIONS( Saturn, Planet )

	public:
		Saturn( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Saturn();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Saturn();
		Saturn( const Saturn& rhs );
		Saturn& operator=( const Saturn& rhs );
	};
}