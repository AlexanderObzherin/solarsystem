#include "Star.h"
#include "AmbientLightingMaterial.h"
#include "Game.h"
#include "GameException.h"

#include "ColorHelper.h"
#include "VectorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "Light.h"
#include "PointLight.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Star )

	const float Star::LightModulationRate = UCHAR_MAX;
	const float Star::LightMovementRate = 10.0f;

	const float Star::AmbientModulationRate = UCHAR_MAX;

	Star::Star( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis )
		:
		AstronomicalObject( game, camera, scale, orbitAxis ),
		mEffect( nullptr ),
		mMaterial( nullptr ),
		mTextureShaderResourceView( nullptr ),
		mVertexBuffer( nullptr ),
		mIndexBuffer( nullptr ),
		mIndexCount( 0 ),
		mKeyboard( nullptr ),
		mAmbientLight(),

		mPointLight( nullptr )

	{
		mPointLight = new PointLight( *mGame );
	}

	Star::~Star()
	{
		DeleteObject( mPointLight );
		DeleteObject( mAmbientLight );
		ReleaseObject( mTextureShaderResourceView );
		DeleteObject( mMaterial );
		DeleteObject( mEffect );
		ReleaseObject( mVertexBuffer );
		ReleaseObject( mIndexBuffer );
	}

	PointLight* Star::GetPointLight()
	{
		return mPointLight;
	}

	void Star::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		// Load the model
		std::unique_ptr<Model> model( new Model( *mGame, "Content\\Models\\Sphere.obj", true ) );

		// Initialize the material
		mEffect = new Effect( *mGame );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\AmbientLighting.cso" );
		mMaterial = new AmbientLightingMaterial();
		mMaterial->Initialize( mEffect );

		Mesh* mesh = model->Meshes().at( 0 );
		mMaterial->CreateVertexBuffer( mGame->Direct3DDevice(), *mesh, &mVertexBuffer );
		mesh->CreateIndexBuffer( &mIndexBuffer );
		mIndexCount = mesh->Indices().size();

		SetObjectTexture();

		mAmbientLight = new Light( *mGame );
		mAmbientLight->SetColor( ColorHelper::White );

		//source of point lighting for other objects
		mPointLight->SetRadius( 1500.0f );

		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		assert( mKeyboard != nullptr );

		//mRenderStateHelper = new RenderStateHelper( *mGame );

		//mSpriteBatch = new SpriteBatch( mGame->Direct3DDeviceContext() );
		//mSpriteFont = new SpriteFont( mGame->Direct3DDevice(), L"Content\\Fonts\\Arial_14_Regular.spritefont" );
	}

	void Star::Update( const GameTime& gameTime )
	{
		AstronomicalObject::Update( gameTime );

		mPointLight->SetPosition( mPosition );
	}

	void Star::Draw( const GameTime& gameTime )
	{
		ID3D11DeviceContext* direct3DDeviceContext = mGame->Direct3DDeviceContext();
		direct3DDeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		Pass* pass = mMaterial->CurrentTechnique()->Passes().at( 0 );
		ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at( pass );
		direct3DDeviceContext->IASetInputLayout( inputLayout );

		UINT stride = mMaterial->VertexSize();
		UINT offset = 0;
		direct3DDeviceContext->IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );
		direct3DDeviceContext->IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

		XMMATRIX worldMatrix = XMLoadFloat4x4( &mWorldMatrix );
		XMMATRIX wvp = worldMatrix * mCamera->ViewMatrix() * mCamera->ProjectionMatrix();

		mMaterial->WorldViewProjection() << wvp;
		mMaterial->ColorTexture() << mTextureShaderResourceView;
		mMaterial->AmbientColor() << mAmbientLight->ColorVector();

		pass->Apply( 0, direct3DDeviceContext );

		direct3DDeviceContext->DrawIndexed( mIndexCount, 0, 0 );
	}

	void Star::UpdateAmbientLight( const GameTime& gameTime )
	{
		static float ambientIntensity = mAmbientLight->Color().a;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->IsKeyDown( DIK_PGUP ) && ambientIntensity < UCHAR_MAX )
			{
				ambientIntensity += AmbientModulationRate * (float)gameTime.ElapsedGameTime();

				XMCOLOR ambientColor = mAmbientLight->Color();
				ambientColor.a = (UCHAR)XMMin<float>( ambientIntensity, UCHAR_MAX );
				mAmbientLight->SetColor( ambientColor );
			}

			if( mKeyboard->IsKeyDown( DIK_PGDN ) && ambientIntensity > 0 )
			{
				ambientIntensity -= AmbientModulationRate * (float)gameTime.ElapsedGameTime();

				XMCOLOR ambientColor = mAmbientLight->Color();
				ambientColor.a = (UCHAR)XMMax<float>( ambientIntensity, 0 );
				mAmbientLight->SetColor( ambientColor );
			}
		}
	}

	void Star::UpdatePointLight( const GameTime& gameTime )
	{
		static float pointLightIntensity = mPointLight->Color().a;
		float elapsedTime = (float)gameTime.ElapsedGameTime();

		// Update point light intensity		
		if( mKeyboard->IsKeyDown( DIK_HOME ) && pointLightIntensity < UCHAR_MAX )
		{
			pointLightIntensity += LightModulationRate * elapsedTime;

			XMCOLOR pointLightLightColor = mPointLight->Color();
			pointLightLightColor.a = (UCHAR)XMMin<float>( pointLightIntensity, UCHAR_MAX );
			mPointLight->SetColor( pointLightLightColor );
		}
		if( mKeyboard->IsKeyDown( DIK_END ) && pointLightIntensity > 0 )
		{
			pointLightIntensity -= LightModulationRate * elapsedTime;

			XMCOLOR pointLightLightColor = mPointLight->Color();
			pointLightLightColor.a = (UCHAR)XMMax<float>( pointLightIntensity, 0.0f );
			mPointLight->SetColor( pointLightLightColor );
		}

		//// Move point light
		//XMFLOAT3 movementAmount = Vector3Helper::Zero;
		//if( mKeyboard != nullptr )
		//{
		//	if( mKeyboard->IsKeyDown( DIK_NUMPAD4 ) )
		//	{
		//		movementAmount.x = -1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_NUMPAD6 ) )
		//	{
		//		movementAmount.x = 1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_NUMPAD9 ) )
		//	{
		//		movementAmount.y = 1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_NUMPAD3 ) )
		//	{
		//		movementAmount.y = -1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_NUMPAD8 ) )
		//	{
		//		movementAmount.z = -1.0f;
		//	}

		//	if( mKeyboard->IsKeyDown( DIK_NUMPAD2 ) )
		//	{
		//		movementAmount.z = 1.0f;
		//	}
		//}

		////XMVECTOR movement = XMLoadFloat3( &movementAmount ) * LightMovementRate * elapsedTime;
		//XMVECTOR velocity = XMLoadFloat3( &mVelocity )
		//XMVECTOR acceleration = XMVectorSet(  );

		//XMStoreFloat3( &mVelocity, movement );

		//mPointLight->SetPosition( mPointLight->PositionVector() + movement );
	}

	void Star::SetObjectCharacteristics()
	{
		//mAxisRotationAngle = 0.0f;
		//mAxisRotationAngleVelocityCoeff = 0.0f;
		//mOrbita = 0.0f;
		//mOrbitRotationAngle = 0.0f;
		//mOrbitRotationAngleVelocityCoeff = 0.0f;
	}

	std::vector<std::string> Star::ObjectDesc()
	{
		return std::vector<std::string>{ "Type : Star" };
	}

	void Star::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\sunmap.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if( FAILED( hr ) )
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}
}