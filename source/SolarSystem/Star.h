#pragma once

#include "AstronomicalObject.h"

using namespace Library;

namespace Library
{
	class Effect;
	class Keyboard;
	class Light;
	class PointLight;
}

namespace Rendering
{
	class AmbientLightingMaterial;

	/*! This class in the astronomical object hierarchy creates shading properties of the object itself, 
	consisting of the ambient lighting material (because the stars shines evenly in all directions, 
	except for example pulsars, etc.), and also it consist of a point light source in the center of the star 
	to determine shading properties of the illuminated objects.
	*/
	class Star : public AstronomicalObject
	{
		RTTI_DECLARATIONS( Star, AstronomicalObject )

	public:
		Star( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis );
		~Star();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectCharacteristics() override;
		virtual void SetObjectTexture() override;
		virtual std::vector<std::string> ObjectDesc();

		PointLight* GetPointLight();
		
	protected:
		Star();
		Star( const Star& rhs );
		Star& operator=(const Star& rhs);

		void UpdateAmbientLight( const GameTime& gameTime );
		void UpdatePointLight( const GameTime& gameTime );

		static const float AmbientModulationRate;

		static const float LightModulationRate;
		static const float LightMovementRate;

		Effect* mEffect;
		AmbientLightingMaterial* mMaterial;
		ID3D11ShaderResourceView* mTextureShaderResourceView;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;
		Keyboard* mKeyboard;

		Light* mAmbientLight;

		PointLight* mPointLight;
	};
}