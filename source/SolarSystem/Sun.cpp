#include "Sun.h"

#include "Game.h"
#include "GameException.h"

#include "ColorHelper.h"
#include "VectorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "Light.h"
#include "PointLight.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Sun )

	Sun::Sun( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis )
		:
		Star( game, camera, scale, orbitAxis )

	{
		SetObjectCharacteristics();
	}

	Sun::~Sun()
	{
	}

	void Sun::Initialize()
	{
		Star::Initialize();
	}

	void Sun::Update( const GameTime& gameTime )
	{
		Star::Update( gameTime );
	}

	void Sun::Draw( const GameTime& gameTime )
	{
		Star::Draw( gameTime );
	}

	void Sun::SetObjectCharacteristics()
	{
		mOrbita = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;
		mOrbitRotationAngleVelocityCoeff = 0.02f;

		mObjectDescription.push_back(std::string{ "Name: Sun" });
		mObjectDescription.push_back(std::string{ "Mass: 1.98855 * 10^30 kg" });
		mObjectDescription.push_back(std::string{ "Equatorial radius: 695700 km" });

		mObjectDescription.push_back(std::string{ "Sidereal rotation period: 25.05 d" });
		mObjectDescription.push_back(std::string{ "Equatorial surface gravity: 274.0 m/s^2 (27.94g)" });
		
		mObjectDescription.push_back(std::string{ "Distance from Milky Way core: 2.7 * 10^17 km" });
		mObjectDescription.push_back(std::string{ "Galactic period: (2.25 - 2.50) * 10^8 yr" });
	}

	std::vector<std::string> Sun::ObjectDesc()
	{
		return mObjectDescription;
	}
}