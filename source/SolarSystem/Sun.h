#pragma once

#include "Star.h"

namespace Rendering
{
	class Sun : public Star
	{
		RTTI_DECLARATIONS( Sun, Star )

	public:
		Sun( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis );
		~Sun();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();

	private:
		Sun();
		Sun( const Sun& rhs );
		Sun& operator=(const Sun& rhs);
	};
}