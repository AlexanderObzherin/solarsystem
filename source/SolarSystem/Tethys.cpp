#include "Tethys.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Tethys )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Tethys::Tethys( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Tethys::~Tethys()
	{
	}

	void Tethys::Initialize()
	{
		Planet::Initialize();
	}

	void Tethys::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Tethys::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Tethys::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\tethys.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Tethys::SetObjectCharacteristics()
	{
		mOrbita = 60.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.02f;

		mObjectDescription.push_back( std::string{ "Name: Tethys" } );
		mObjectDescription.push_back( std::string{ "Mass: 6.17449 * 10^20 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 531.1 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 0.146 m/s^2" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 1.887802 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 1.887802 d" } );
	}

	std::vector<std::string> Tethys::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Tethys::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}