#pragma once

#include "Planet.h"

namespace Rendering
{
	class Tethys : public Planet
	{
		RTTI_DECLARATIONS( Tethys, Planet )

	public:
		Tethys( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Tethys();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Tethys();
		Tethys( const Tethys& rhs );
		Tethys& operator=( const Tethys& rhs );
	};
}