#include "Titan.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Titan )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Titan::Titan( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Titan::~Titan()
	{
	}

	void Titan::Initialize()
	{
		Planet::Initialize();
	}

	void Titan::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Titan::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Titan::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\titan.png";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Titan::SetObjectCharacteristics()
	{
		mOrbita = 110.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = 0.01f;

		mOrbitRotationAngle = XM_PI;
		mOrbitRotationAngleVelocityCoeff = 0.009f;

		mObjectDescription.push_back( std::string{ "Name: Titan" } );
		mObjectDescription.push_back( std::string{ "Mass: 1.3452 * 10^23 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 2575.5 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 1.352 m/s^2 (0.14g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 15.945 d" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: 15.945 d" } );
	}

	std::vector<std::string> Titan::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Titan::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}