#pragma once

#include "Planet.h"

namespace Rendering
{
	class Titan : public Planet
	{
		RTTI_DECLARATIONS( Titan, Planet )

	public:
		Titan( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Titan();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Titan();
		Titan( const Titan& rhs );
		Titan& operator=( const Titan& rhs );
	};
}