#include "Uranus.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Uranus )

		//const float Planet::LightModulationRate = UCHAR_MAX;
		//const float Planet::LightMovementRate = 10.0f;

		Uranus::Uranus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		scaleCoefficient = scale;
		SetObjectCharacteristics();
	}

	Uranus::~Uranus()
	{
	}

	void Uranus::Initialize()
	{
		Planet::Initialize();
	}

	void Uranus::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}

	void Uranus::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Uranus::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\uranus.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView );
		if (FAILED( hr ))
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}
	}

	void Uranus::SetObjectCharacteristics()
	{
		mOrbita = 850.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = -0.04f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.005f;

		mObjectDescription.push_back( std::string{ "Name: Uranus" } );
		mObjectDescription.push_back( std::string{ "Mass: 8.6810 * 10^25 kg" } );
		mObjectDescription.push_back( std::string{ "Radius: 25362 km" } );
		mObjectDescription.push_back( std::string{ "Surface gravity: 8.69 m/s^2 (0.886g)" } );
		mObjectDescription.push_back( std::string{ "Orbital period: 84.0205 yr" } );
		mObjectDescription.push_back( std::string{ "Sidereal rotation period: -0.71833 d" } );
	}

	std::vector<std::string> Uranus::ObjectDesc()
	{
		return mObjectDescription;
	}

	float Uranus::GetScaleCoeff()
	{
		return scaleCoefficient;
	}
}