#pragma once

#include "Planet.h"

namespace Rendering
{
	class Uranus : public Planet
	{
		RTTI_DECLARATIONS( Uranus, Planet )

	public:
		Uranus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Uranus();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string> ObjectDesc();
		virtual float GetScaleCoeff();

	private:
		Uranus();
		Uranus( const Uranus& rhs );
		Uranus& operator=( const Uranus& rhs );
	};
}