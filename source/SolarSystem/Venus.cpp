#include "Venus.h"
#include "PointLightMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "Sun.h"
#include "PointLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "ProxyModel.h"
#include "RenderStateHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	RTTI_DEFINITIONS( Venus )

	//const float Planet::LightModulationRate = UCHAR_MAX;
	//const float Planet::LightMovementRate = 10.0f;

	Venus::Venus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource )
		:
		Planet( game, camera, scale, orbitAxis, mPointLightSource )
	{
		SetObjectCharacteristics();
	}

	Venus::~Venus()
	{
	}

	void Venus::Initialize()
	{
		Planet::Initialize();
	}

	void Venus::Update( const GameTime& gameTime )
	{
		Planet::Update( gameTime );
	}
	
	void Venus::Draw( const GameTime& gameTime )
	{
		Planet::Draw( gameTime );
	}

	void Venus::SetObjectTexture()
	{
		std::wstring textureName = L"Content\\Textures\\venus.jpg";
		HRESULT hr = DirectX::CreateWICTextureFromFile(mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), textureName.c_str(), nullptr, &mTextureShaderResourceView);
		if (FAILED(hr))
		{
			throw GameException("CreateWICTextureFromFile() failed.", hr);
		}
	}

	void Venus::SetObjectCharacteristics()
	{
		mOrbita = 90.0f;

		mAxisRotationAngle = 0.0f;
		mAxisRotationAngleVelocityCoeff = -0.04f;

		mOrbitRotationAngle = 0.0f;
		mOrbitRotationAngleVelocityCoeff = 0.02f;

		mObjectDescription.push_back(std::string{ "Name: Venus" });
		mObjectDescription.push_back(std::string{ "Mass: 4.8675 * 10^24 kg" });
		mObjectDescription.push_back(std::string{ "Radius: 6051.8 km" });
		mObjectDescription.push_back(std::string{ "Surface gravity: 8.87 m/s^2 (0.904g)" });
		mObjectDescription.push_back(std::string{ "Orbital period: 224.701 d" });
		mObjectDescription.push_back(std::string{ "Sidereal rotation period: -243.025 d" });
	}

	std::vector<std::string> Venus::ObjectDesc()
	{
		return mObjectDescription;
	}
}