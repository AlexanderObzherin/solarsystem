#pragma once

#include "Planet.h"

namespace Rendering
{
	class Venus : public Planet
	{
		RTTI_DECLARATIONS( Venus, Planet )

		public:
		Venus( Game& game, Camera& camera, float scale, const XMFLOAT3& orbitAxis, const PointLight& mPointLightSource );
		~Venus();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void SetObjectTexture() override;
		virtual void SetObjectCharacteristics() override;
		virtual std::vector<std::string>ObjectDesc();

	private:
		Venus();
		Venus( const Venus& rhs );
		Venus& operator=(const Venus& rhs);
	};
}